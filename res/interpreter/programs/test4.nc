// 45 deg cicular arcs in relative coordinates
// Jorge correa
// 2017


N10 G90 G0 X0 Y0 Z0                    // coordinate home
N20 G91 F50
G02 X2.9289  Y7.0711  I10 J0           // first quadrant
G02 X7.0711  Y2.9289  I7.0711 J-7.0711  
G92
G03 X7.0711  Y2.9289  I0 J10
G03 X2.9289  Y7.0711  I-7.0711 J7.0711
G92 
G03 X-2.9289 Y7.0711 I-10 J0 F300      // first quadrant
G03 X-7.0711 Y2.9289 I-7.0711 J-7.0711
G92
G02 X-7.0711 Y2.9289  I0 J10
G02 X-2.9289 Y7.0711  I7.0711 J7.0711
G92
G03 X-7.0711 Y-2.9289 I0 J-10
G03 X-2.9289 Y-7.0711 I7.0711 J-7.0711
G92
G02 X-2.9289 Y-7.0711 I-10 J0 F300
G02 X-7.0711 Y-2.9289 I-7.0711 J7.0711
G92
G03 X2.9289  Y-7.0711  I10 J0
G03 X7.0711  Y-2.9289  I7.0711 J7.0711
G92
G02 X7.0711  Y-2.9289 I0 J-10
G02 X2.9289  Y-7.0711 I-7.0711 J-7.0711
